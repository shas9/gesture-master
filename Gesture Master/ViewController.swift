//
//  ViewController.swift
//  Gesture Master
//
//  Created by Kite Games Studio on 25/8/21.
//

import UIKit

class ViewController: UIViewController {


    @IBOutlet weak var myView: UIView!
    @IBOutlet weak var labelInView: UILabel!
    
    
    var tapped: Bool = false
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        
        let tap = UITapGestureRecognizer(target: self, action: #selector(self.handleTap(_:)))
        myView.addGestureRecognizer(tap)
        tap.delegate = self
        
        let pinch = UIPinchGestureRecognizer(target: self, action: #selector(self.handlePinch(_:)))
        myView.addGestureRecognizer(pinch)
        pinch.delegate = self
        
        let rotate = UIRotationGestureRecognizer(target: self, action: #selector(self.handleRotate(_:)))
        myView.addGestureRecognizer(rotate)
        rotate.delegate = self
        
        let pan = UIPanGestureRecognizer(target: self, action: #selector(self.handlePan(_:)))
        myView.addGestureRecognizer(pan)
        pan.delegate = self
    }
    
    @objc func handleTap(_ gesture: UITapGestureRecognizer) {
        
        if tapped {
            myView.backgroundColor = UIColor.red
            labelInView.text = "TAP AGAIN!"
            tapped = false
            
            return
        }
        myView.backgroundColor = UIColor.green
        labelInView.text = "You Tapped Me!"
        tapped = true
    }
    
    @objc func handlePinch(_ gesture: UIPinchGestureRecognizer) {
        
        guard let gestureView = gesture.view else {
          return
        }

        gestureView.transform = gestureView.transform.scaledBy(
          x: gesture.scale,
          y: gesture.scale
        )
        gesture.scale = 1
    }
    
    @objc func handleRotate(_ gesture: UIRotationGestureRecognizer) {
        guard let gestureView = gesture.view else {
          return
        }

        gestureView.transform = gestureView.transform.rotated(
          by: gesture.rotation
        )
        
        gesture.rotation = 0

    }
    
    @objc func handlePan(_ gesture: UIPanGestureRecognizer) {
        // 1
        let translation = gesture.translation(in: view)

        // 2
        guard let gestureView = gesture.view else {
          return
        }

        gestureView.center = CGPoint(
          x: gestureView.center.x + translation.x,
          y: gestureView.center.y + translation.y
        )

        // 3
        gesture.setTranslation(.zero, in: view)
    }

}

extension ViewController: UIGestureRecognizerDelegate {
  func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer, shouldRecognizeSimultaneouslyWith otherGestureRecognizer: UIGestureRecognizer
  ) -> Bool {
    return true
  }

}
